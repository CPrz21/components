import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2("https://a-static.besthdwallpaper.com/goku-masterizado-ultra-instinto-papel-pintado-2880x1800-22088_8.jpg"),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2("https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/0cc85956-6607-47c8-8e80-4db51c05bcff/dceesgg-77e8b045-2e92-4b13-b7d3-cd0bdd9031f8.jpg?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzBjYzg1OTU2LTY2MDctNDdjOC04ZTgwLTRkYjUxYzA1YmNmZlwvZGNlZXNnZy03N2U4YjA0NS0yZTkyLTRiMTMtYjdkMy1jZDBiZGQ5MDMxZjguanBnIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.Xawh9fkoFHyY7ZUHyW279i86YdrIGL38x4qqFzi3uIA"),
          SizedBox(height: 30.0),
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2("https://i.imgur.com/GyGiNdT.jpg"),
          SizedBox(height: 30.0),
        ],
      ),
    );
  }

  Widget _cardTipo1() {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title: Text('Titulo de Card'),
            subtitle: Text('descripcion de la card creada'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(onPressed: (){}, child: Text('Cancelar', style: TextStyle(color: Colors.blue))),
              FlatButton(onPressed: (){}, child: Text('Aceptar', style: TextStyle(color: Colors.blue))),
            ],
          )
        ],
      ),
    );
  }

  Widget _cardTipo2(String url){
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
      clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'),
            image: NetworkImage(url),
            height: 280.0,
            fit: BoxFit.cover,
            fadeInDuration: Duration(milliseconds: 300),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text("El mero mero goku")
          )
        ],
      ),
    );

    // return Container(
    //   decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color:Colors.red,),
    //   child: ClipRRect(child: card, borderRadius: BorderRadius.circular(20.0),)
    // );
  }

}