import 'package:flutter/material.dart';

class AvatarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Avatar Page'),
        actions: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: ClipOval(
                child: FadeInImage(
                  placeholder: AssetImage("assets/jar-loading.gif"),
                  image: NetworkImage(
                      "https://cdn.dribbble.com/users/17559/screenshots/6664357/figma.png"),
                  width: 40.0,
                  height: 40.0,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(right: 10.0),
            child: CircleAvatar(
              child: Text('CP'),
              backgroundColor: Colors.teal,
            ),
          )
        ],
      ),
      body: Center(
        child: FadeInImage(
            fadeInDuration: Duration(milliseconds: 200),
            placeholder: AssetImage('assets/jar-loading.gif'),
            image: NetworkImage(
                "https://assets.reviews.com/uploads/2018/07/10050434/Featured-Image-for-Puppies-900x585.jpg")),
      ),
    );
  }
}
