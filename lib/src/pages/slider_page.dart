import 'package:flutter/material.dart';

class SliderPage extends StatefulWidget {
  @override
  _SliderPageState createState() => _SliderPageState();
}

class _SliderPageState extends State<SliderPage> {
  double _sliderValue = 100.0;
  bool _blockCheck = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Sliders'),
      ),
      body: Container(
          padding: EdgeInsets.only(top: 50.0),
          child: Column(
            children: <Widget>[
              _crearSlider(),
              _checkBox(),
              _crearSwitch(),
              Expanded(child: _crearImagen()),
            ],
          )),
    );
  }

  Widget _crearSlider() {
    return Slider(
      activeColor: Colors.redAccent,
      inactiveColor: Colors.grey,
      label: 'Tamaño de imagen ${_sliderValue.toInt()}',
      // divisions: 10,
      value: _sliderValue,
      min: 10.0,
      max: 400,
      onChanged:
          _blockCheck ? null : (value) => setState(() => _sliderValue = value),
    );
  }

  Widget _checkBox() {
    return CheckboxListTile(
        title: Text('Bloquear Slider'),
        value: _blockCheck,
        onChanged: (value) {
          setState(() {
            _blockCheck = value;
          });
        });
  }

  Widget _crearSwitch() {
    return SwitchListTile(
        title: Text('Bloquear Slider'),
        value: _blockCheck,
        onChanged: (value) {
          setState(() {
            _blockCheck = value;
          });
        });
  }

  Widget _crearImagen() {
    return Image(
      image: NetworkImage(
          "https://vignette.wikia.nocookie.net/death-battle-fanon-wiki-en-espanol/images/0/09/Goku.png/revision/latest?cb=20190222052804&path-prefix=es"),
      width: _sliderValue,
    );
  }
}
